import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import { MessagesListComponent } from './messages-list.component';
import {FormsModule} from '@angular/forms';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {ConfigurationService, LoadingService, SharedModule} from '@universis/common';
import { ApiTestingModule, ApiTestingController } from '@universis/common/testing';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {MostModule} from '@themost/angular';
import {TestingConfigurationService} from '@universis/common/testing';
import {MessagesService} from '../../services/messages.service';
import {MessageSharedService} from '../../../students-shared/services/messages.service';
import {NgPipesModule} from 'ngx-pipes';
import {ModalModule} from 'ngx-bootstrap';
import {APP_BASE_HREF} from '@angular/common';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('MessagesListComponent', () => {
  const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading' , 'hideLoading' ]);

  let mockApi: ApiTestingController;
  let component: MessagesListComponent;
  let fixture: ComponentFixture<MessagesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        FormsModule,
        TranslateModule.forRoot(),
        InfiniteScrollModule,
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        SharedModule,
        NgPipesModule,
        ModalModule.forRoot(),
        ApiTestingModule.forRoot()
      ],
      providers: [
        MessagesService,
        MessageSharedService,
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        {
          provide: LoadingService,
          useValue: loadingSvc
        },
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        }
      ],
      declarations: [ MessagesListComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    // get api test controller
    mockApi = TestBed.get(ApiTestingController);

    mockApi.match({
      url: '/api/students/me/messages/',
      method: 'GET'
    }).map(request => {
      request.flush({
        value: []
      });
    });

    fixture = TestBed.createComponent(MessagesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
