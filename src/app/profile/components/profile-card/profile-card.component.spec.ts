import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserService} from '@universis/common';
import {ProfileService} from '../../services/profile.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import { ConfigurationService } from '@universis/common';
import {TranslateModule} from '@ngx-translate/core';
import { of } from 'rxjs';

import {ProfileCardComponent} from './profile-card.component';
import {TestingConfigurationService} from '../../../test';

describe('ProfileCardComponent', () => {
    let component: ProfileCardComponent;
    let fixture: ComponentFixture<ProfileCardComponent>;

    const profileSvc = jasmine.createSpyObj('ProfileService', ['getStudent']);
    const userSvc = jasmine.createSpyObj('UserService' , ['getUser', 'getProfile', 'checkLogin']);
    userSvc.getProfile.and.returnValue(Promise.resolve(JSON.parse('{}')));
    profileSvc.getStudent.and.returnValue(Promise.resolve(JSON.parse('{}')));

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [
                ProfileCardComponent
            ],
            imports: [
                HttpClientTestingModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                })],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                },
                {
                    provide: ProfileService,
                    useValue: profileSvc
                },
                {
                    provide: UserService,
                    useValue: userSvc
                },
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProfileCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
