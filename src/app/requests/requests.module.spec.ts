import {TestBed, async, inject} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {RequestsModule} from './requests.module';
import {RequestsSharedModule} from './requests-shared.module';
describe('RequestsModule', () => {
  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ]
    }).compileComponents();
  }));
  it('should create an instance', inject([TranslateService], (translateService: TranslateService) => {
    const requestsSharedModule = new RequestsSharedModule(translateService);
    expect(requestsSharedModule).toBeTruthy();
  }));
});
